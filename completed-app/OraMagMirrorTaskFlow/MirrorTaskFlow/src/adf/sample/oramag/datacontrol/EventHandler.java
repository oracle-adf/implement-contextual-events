package adf.sample.oramag.datacontrol;


import adf.sample.oramag.beans.MirrorPageBean;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.context.FacesContext;

import oracle.adf.model.binding.DCBindingContainerValueChangeEvent;
import oracle.adf.view.rich.context.AdfFacesContext;

/**
 * JavaBean class exposing method to call from contextual event handlers. Because
 * contextual event is a functionality of the ADF binding layer, all event handlers
 * must be exposed from a Data Control. In this use case, the exposing Data Control 
 * is a JavaBean data control created from this class
 * 
 * @author Frank Nimphius, for Oracle Magazine 05/06 2011
 */
public class EventHandler {

    public EventHandler() {
        super();
    }

    /**
     * Method that shows how to handle the native payLoad object, which is the 
     * event itself by default. 
     * 
     * If the value change event is raised from an attribute binding in the PageDef file 
     * then the payload object is of type DCBindingContainerValueChangeEvent. If the even 
     * is raised by a JSF action, then the default payload object is the JSF ActionEevent. 
     * Similar for ValueChange, Query, Selection etc.
     * 
     * @param customPayLoad
     */
    public void handleEventObjectPayload(DCBindingContainerValueChangeEvent customPayLoad) {
        String changedDepartmentName =
            (String)customPayLoad.getNewValue();
        handleEventStringPayload(changedDepartmentName);
    }

   /**
     * A payload may be customized to send a String instead of the event object. This
     * is handled by this method in the example. 
     * @param customPayLoad
     */
    public void handleEventStringPayload(String customPayLoad) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elctx = fctx.getELContext();
        ExpressionFactory exprFactory =
            fctx.getApplication().getExpressionFactory();
        ValueExpression ve =
            exprFactory.createValueExpression(elctx, "#{backingBeanScope.mirrorPageBean}",
                                              Object.class);
        MirrorPageBean mirrorPageBean = (MirrorPageBean)ve.getValue(elctx);
        mirrorPageBean.setMirrorValue(customPayLoad);
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.getCurrentInstance().addPartialTarget(mirrorPageBean.getMirrorTextField());
    }


}
