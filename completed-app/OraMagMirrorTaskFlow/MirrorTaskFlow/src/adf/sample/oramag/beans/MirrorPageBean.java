package adf.sample.oramag.beans;

import oracle.adf.view.rich.component.rich.input.RichInputText;

public final class MirrorPageBean {
    String mirrorValue = null;
    private RichInputText mirrorTextField;

    public MirrorPageBean() {
        super();
    }

    public void setMirrorValue(String mirrorValue) {
        this.mirrorValue = mirrorValue;
    }

    public String getMirrorValue() {
        return mirrorValue;
    }

    public void setMirrorTextField(RichInputText mirrorTextField) {
        this.mirrorTextField = mirrorTextField;
    }

    public RichInputText getMirrorTextField() {
        return mirrorTextField;
    }
}
